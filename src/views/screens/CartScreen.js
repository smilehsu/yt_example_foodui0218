import { StyleSheet, Text, View, SafeAreaView, ScrollView, Image, FlatList } from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from "../../consts/colors"
import { PrimaryButton } from '../components/Button';
import foods from '../../consts/foods';


const CartScreen = ({ navigation }) => {
    // 購物車資料
    const CartCard = ({ item }) => {
        return (
            <View style={styles.CartCard}>
                {/* 圖片 */}
                <Image source={item.image}
                    style={{ width: 80, height: 80 }}
                />
                {/* 名稱跟價錢 */}
                <View style={{ flex: 1, height: 100, marginLeft: 10, paddingVertical: 20 }}>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>{item.name}</Text>
                    <Text style={{ fontSize: 13, color: COLORS.grey }}>{item.ingredients}</Text>
                    <Text style={{ fontSize: 18, fontWeight: "bold" }}>${item.price}</Text>
                </View>
                {/* 訂購數量跟按鈕 */}
                <View style={{ marginLeft: 20, alignItems: "center" }}>
                    <Text style={{ fontSize: 18, fontWeight: "bold" }}>3</Text>
                    <View style={styles.ActionBtn}>
                        <Icon name="remove" size={25} color={COLORS.white} />
                        <Icon name="add" size={25} color={COLORS.white} />
                    </View>
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
            <View style={styles.Header}>
                <Icon name="arrow-back-ios" size={28} onPress={navigation.goBack} />
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>Cart</Text>
            </View>
            {/* 購物車清單 */}
            <FlatList showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 80 }}
                data={foods}
                renderItem={({ item }) => <CartCard item={item} />}
                ListFooterComponentStyle={{ paddingHorizontal: 20, marginTop: 20 }}
                ListFooterComponent={() => (
                    <View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 15 }}>
                            <Text style={{ fontSize: 18, fontWeight: "bold" }}>Total Price</Text>
                            <Text style={{ fontSize: 18, fontWeight: "bold" }}>$50</Text>
                        </View>
                        <View style={{ marginTop: 30 }}>
                            <PrimaryButton title="CHECKOUT" />
                        </View>
                    </View>
                )}
            />
        </SafeAreaView >
    )
}



{/* <PrimaryButton title="Get Started"
onPress={() => navigation.navigate("Home")}
/> */}

const styles = StyleSheet.create({
    Header: {
        flexDirection: "row",
        paddingHorizontal: 20,
        alignItems: "center",
        marginHorizontal: 20,
    },
    CartCard: {
        flexDirection: "row",
        height: 100,
        elevation: 15,
        borderRadius: 10,
        backgroundColor: COLORS.white,
        marginVertical: 10,
        marginHorizontal: 20,
        paddingHorizontal: 10,
        alignItems: "center",
    },
    ActionBtn: {
        flexDirection: "row",
        width: 80,
        height: 30,
        backgroundColor: COLORS.primary,
        borderRadius: 30,
        paddingHorizontal: 5,
        justifyContent: "space-between",
        alignItems: "center"
    },
})

export default CartScreen