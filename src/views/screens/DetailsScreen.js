import { StyleSheet, Text, View, SafeAreaView, ScrollView, Image } from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from "../../consts/colors"
import { OrderButton } from '../components/Button';


const DetailsScreen = ({ navigation, route }) => {
    // 設定 item變數 來接收 上一頁傳來的參數
    const item = route.params;
    // 印出 上一頁傳來的參數 測試用
    // console.log(item)

    return (
        <SafeAreaView style={{ backgroundColor: COLORS.white }}>
            <View style={styles.Header}>
                <Icon name="arrow-back-ios" size={28} onPress={() => navigation.goBack()} />
                <Text style={{ fontSize: 20, fontWeight: "bold" }}>Details</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                {/* 食物大圖 */}
                <View style={{ justifyContent: "center", alignItems: "center", height: 280 }}>
                    <Image source={item.image} style={{ width: 220, height: 220, }} />
                </View>
                {/* 食物簡介 */}
                <View style={styles.Details}>
                    {/* 標題 */}
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Text style={{ fontSize: 20, fontWeight: "bold", color: COLORS.white }}>{item.name}</Text>
                        <View style={styles.IconContainer}>
                            <Icon name="favorite-border" size={24} style={{ color: COLORS.primary }} />
                        </View>
                    </View>
                    {/* 文字區塊 */}
                    <Text style={styles.DetailsText}>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veritatis id labore architecto eius necessitatibus hic beatae, esse quia provident doloribus. Minima nesciunt a sint eaque dolor illo blanditiis aut corporis.</Text>
                    {/* 訂購按鈕 */}
                    <View style={{ marginTop: 40, marginBottom: 40 }}>
                        <OrderButton title="Add to Cart" />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView >
    )
}

export default DetailsScreen

const styles = StyleSheet.create({
    Header: {
        flexDirection: "row",
        paddingHorizontal: 20,
        alignItems: "center",
        marginHorizontal: 20,
    },
    Details: {
        paddingHorizontal: 20,
        paddingTop: 40,
        paddingBottom: 60,
        backgroundColor: COLORS.primary,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
    },
    IconContainer: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: COLORS.white,
        justifyContent: "center",
        alignItems: "center"
    },
    DetailsText: {
        marginTop: 10,
        lineHeight: 22,
        fontSize: 16,
        color: COLORS.white
    }
})