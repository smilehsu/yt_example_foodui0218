import { StyleSheet, Text, View, Image, Button } from 'react-native';
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import COLORS from '../../consts/colors';

import { PrimaryButton } from '../components/Button';



// 登入畫面
const OnBoardScreen = ({ navigation }) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
            <View style={{ height: 400, }}>
                <Image
                    source={require("../../assets/onboardImage.png")}
                    style={{ width: "100%", resizeMode: "contain", top: -180 }}
                />
            </View>
            <View style={styles.TextContainer}>
                <View>
                    <Text style={{ fontSize: 32, fontWeight: "bold", textAlign: "center" }}>Delicious Food</Text>
                    <Text style={{
                        marginTop: 20,
                        fontSize: 18,
                        textAlign: "center",
                        color: COLORS.grey
                    }}> We help you to find best and delicious food</Text>
                </View>
                {/* indicator container */}
                <View style={styles.IndicatorContainer}>
                    <View style={styles.CurrentIndicator}></View>
                    <View style={styles.Indicator}></View>
                    <View style={styles.Indicator}></View>
                </View>
                <PrimaryButton title="Get Started"
                    onPress={() => navigation.navigate("Home")}
                />

            </View>
        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    TextContainer: {
        flex: 1,
        paddingHorizontal: 50,
        justifyContent: "space-between",
        paddingBottom: 40,

    },
    IndicatorContainer: {
        flex: 1,
        height: 50,
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center",
    },
    CurrentIndicator: {
        width: 30,
        height: 12,
        borderRadius: 10,
        backgroundColor: COLORS.primary,
        marginHorizontal: 5,
    },
    Indicator: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: COLORS.grey,
        marginHorizontal: 5,
    }
})


export default OnBoardScreen