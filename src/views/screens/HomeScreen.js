import { SafeAreaView, StyleSheet, Text, View, Image, TextInput, ScrollView, TouchableOpacity, FlatList, Dimensions } from 'react-native'
import React, { useState } from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from "../../consts/colors"
import categories from '../../consts/categories'
import foods from '../../consts/foods';

const { width } = Dimensions.get("screen")
const cardWidth = width / 2 - 20

const HomeScreen = ({ navigation }) => {

    const [selectedCategoryIndex, setSelectedCategoryIndex] = useState(0)

    // 項目清單
    const ListCategories = () => {
        return (
            <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={styles.CategoriesListContainer}
            >
                {/* 注意這邊的寫法 常用 但都記不住 XD */}
                {categories.map((category, index) => (
                    <TouchableOpacity key={index}
                        activeOpacity={0.8}
                        // 點選後 給index的值 (與下面的判斷式配合使用)
                        onPress={() => setSelectedCategoryIndex(index)}
                    >
                        <View style={{
                            // 選取中的分類 與未選中的 背景色 判斷式
                            backgroundColor: selectedCategoryIndex == index
                                ? COLORS.primary
                                : COLORS.secondary
                            , ...styles.CategoryBtn
                        }}>
                            <View style={styles.CategoryBtnImg}>
                                <Image source={category.image}
                                    style={{ width: 35, height: 35, resizeMode: "cover" }}
                                />
                            </View>
                            {/* 被選中的顏色改變 判斷式 */}
                            <Text style={{
                                fontSize: 15, fontWeight: "bold", marginLeft: 10,
                                color: selectedCategoryIndex == index
                                    ? COLORS.white
                                    : COLORS.primary
                            }}>{category.name}</Text>
                        </View>
                    </TouchableOpacity>
                ))
                }
            </ScrollView >
        )
    }
    // 食物的卡片
    const Card = ({ food }) => {
        return (
            // navigation.navigate 這邊傳遞的 DetailsScreen 要先在App.js裡面設定
            <TouchableOpacity underlayColor={COLORS.white} activeOpacity={0.9} onPress={() => navigation.navigate("DetailsScreen", food)}>
                <View style={styles.Card}>
                    <View style={{ alignItems: "center", top: -40 }}>
                        <Image source={food.image}
                            style={{
                                width: 120,
                                height: 120,
                            }}
                        />
                    </View>
                    <View style={{ marginHorizontal: 20 }}>
                        <Text style={{ fontSize: 17, fontWeight: "bold" }}>{food.name}</Text>
                        <Text style={{ fontSize: 14, color: COLORS.grey, marginTop: 2 }}>{food.ingredients}</Text>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 10, marginHorizontal: 20, justifyContent: "space-between" }}>
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>${food.price}</Text>
                        <View style={styles.AddToCartBtn}>
                            <Icon name="add" size={20} color={COLORS.white} />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
            <View style={styles.Header}>
                <View>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: 28, }}>Hello,</Text>
                        <Text style={{ fontSize: 28, fontWeight: "bold", marginLeft: 10 }}>Hsu</Text>
                    </View>
                    <Text style={{ marginTop: 5, fontSize: 22, color: COLORS.grey }}>What do ypu want today?</Text>
                </View>
                {/* 頭像 */}
                <Image source={require("../../assets/person.png")}
                    style={{ width: 50, height: 50, borderRadius: 25 }}
                />
            </View>
            {/* 搜尋列 */}
            <View style={{ marginTop: 40, flexDirection: "row", paddingHorizontal: 20 }}>
                <View style={styles.InputContainer}>
                    <Icon name="search" size={28} />
                    <TextInput
                        placeholder='請輸入餐點名稱'
                        style={{ flex: 1, fontSize: 18, marginLeft: 5 }}
                    />
                </View>
                {/* 排序按鈕 純展示 無作用 */}
                <View style={styles.SortBtn}>
                    <Icon name="tune" size={28} color={COLORS.white} />
                </View>
            </View>
            {/* 菜單項目的分類欄 */}
            <View>
                <ListCategories />
            </View>
            {/* 卡片 */}
            <FlatList
                showsVerticalScrollIndicator={false}
                numColumns={2}
                data={foods}
                renderItem={({ item }) => <Card food={item} />}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    Header: {
        marginTop: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 20,

    },
    InputContainer: {
        flex: 1,
        flexDirection: "row",
        height: 50,
        borderRadius: 10,
        backgroundColor: COLORS.light,
        alignItems: "center",
        paddingHorizontal: 20,
    }, SortBtn: {
        width: 50,
        height: 50,
        marginLeft: 10,
        backgroundColor: COLORS.primary,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    CategoriesListContainer: {
        paddingVertical: 30,
        paddingHorizontal: 20,
        alignItems: "center",
    },
    CategoryBtn: {
        flexDirection: "row",
        width: 120,
        height: 50,
        marginRight: 7,
        borderRadius: 30,
        alignItems: "center",
        paddingHorizontal: 5,

    },
    CategoryBtnImg: {
        width: 35,
        height: 35,
        backgroundColor: COLORS.white,
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    Card: {
        width: cardWidth,
        height: 220,
        marginHorizontal: 10,
        marginTop: 50,
        marginBottom: 20,
        borderRadius: 15,
        elevation: 13,
        backgroundColor: COLORS.white,
    },
    AddToCartBtn: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: COLORS.primary,
        justifyContent: "center",
        alignItems: "center",
    },
})

export default HomeScreen

