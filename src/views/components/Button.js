import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import COLORS from '../../consts/colors'

const PrimaryButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            <View style={styles.BtnContainer}>
                <Text style={styles.Title}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const OrderButton = ({ title, onPress = () => { } }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onPress}
        >
            {/* 這邊注意 套用 多重樣式的寫法 */}
            <View style={{ ...styles.BtnContainer, backgroundColor: COLORS.white }}>
                <Text style={{ ...styles.Title, color: COLORS.primary }}>{title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    Title: { color: COLORS.white, fontWeight: 'bold', fontSize: 18 },
    BtnContainer: {
        backgroundColor: COLORS.primary,
        height: 60,
        borderRadius: 30,
        justifyContent: "center",
        alignItems: "center",
    },

})

// export default Button
//注意這邊 的匯出方式
export { PrimaryButton, OrderButton }