import "react-native-gesture-handler"
import React from 'react'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from "../../consts/colors"
import { View } from "react-native"


import HomeScreen from '../screens/HomeScreen';
import CartScreen from '../screens/CartScreen';



const Tab = createBottomTabNavigator();

// tab 的 options 影片是舊版的 大部分已失效 
// 原本是 tabBarOptions  改為 screenOptions 
// 定義屬性的方式 也改變
// 參考官網文件 https://reactnavigation.org/docs/bottom-tab-navigator/#options
// 影片教學 Bottom Tab Navigation with Animation | React-Navigation v6/5 | Part-1 https://youtu.be/XiutL0uLICg

const BottomNavigator = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarStyle: {
                    hight: 55,
                    //目前只有這個看得出有效果 上下兩個看不出來...
                    borderTopWidth: 0,
                    elevation: 1,
                },
                // 隱藏 標題列
                headerShown: false,

                tabBarActiveTintColor: COLORS.primary

            }}>
            {/* 頁籤1 HomeScreen */}
            <Tab.Screen name="HomeScreen" component={HomeScreen} options={{ tabBarShowLabel: false, tabBarIcon: ({ color }) => (<Icon name="home" color={color} size={28} />) }} />
            {/* 頁籤2 LocalMall */}
            <Tab.Screen name="LocalMall" component={HomeScreen} options={{ tabBarShowLabel: false, tabBarIcon: ({ color }) => (<Icon name="local-mall" color={color} size={28} />) }} />
            {/* 頁籤3 Search */}
            <Tab.Screen name="Search" component={HomeScreen} options={{
                tabBarShowLabel: false, tabBarIcon: ({ color }) => (
                    <View style={{
                        width: 60,
                        height: 60,
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: COLORS.white,
                        borderColor: COLORS.primary,
                        borderWidth: 2,
                        borderRadius: 30,
                        top: -25,
                        elevation: 5,
                    }}>
                        <Icon name="search" color={COLORS.primary} size={28} />
                    </View>
                )
            }} />
            {/* 頁籤4 Favorite */}
            <Tab.Screen name="Favorite" component={HomeScreen} options={{ tabBarShowLabel: false, tabBarIcon: ({ color }) => (<Icon name="favorite" color={color} size={28} />) }} />
            {/* 頁籤5 Cart */}
            <Tab.Screen name="Cart" component={CartScreen} options={{ tabBarShowLabel: false, tabBarIcon: ({ color }) => (<Icon name="shopping-cart" color={color} size={28} />) }} />
        </Tab.Navigator>
    )
}

export default BottomNavigator